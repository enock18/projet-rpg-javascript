export class Character {
    protected name: string;
    protected health: number;
    protected hitStrength: number;
    protected lvl: number;
    protected xp: number;
    protected theAttack: number
    constructor(
        name: string,
        health: number,
        hitStrength: number,
        lvl: number,
        xp: number,
    ) {
        this.name = name;
        this.health = health;
        this.hitStrength = hitStrength;
        this.lvl = lvl;
        this.xp = xp;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getHealth(): number {
        return this.health;
    }

    public setHealth(health: number): void {
        this.health = health;
    }

    public attack(): number {
        return this.hitStrength * this.lvl;
    }

    public die() {
        this.health = 0;
    }

    public getLevel(): number {
        return this.lvl;
    }

    public setLevel(lvl: number): void {
        this.lvl = lvl;
    }

    public getXp(): number {
        return this.xp;
    }

    public setXp(xp: number): void {
        this.xp = xp;
    }

    public getHitStrength(): number {
        return this.hitStrength;
    }

    public setHitStrength(hitStrength: number): void {
        this.hitStrength = hitStrength;
    }
}
