import { Character } from './Character';

export class Hero extends Character {
  protected race: string;
  constructor(
    name: string,
    health: number,
    hitStrength: number,
    lvl: number,
    xp: number
  ) {
    super(name, health, hitStrength, lvl, xp);
  }

  getRace() {
    return this.race;
  }

  setRace(race: string) {
    this.race = race;
  }
}
