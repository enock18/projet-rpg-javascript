export class Battle {

    public oponentOne: any;
    public oponentTwo: any;

    constructor(oponentOne: any, oponentTwo: any) {
        this.oponentOne = oponentOne;
        this.oponentTwo = oponentTwo;
    }

    fightStart() {
        let numberAttack: number = 1;
        const history = []
        const winner = [];
        const num: number = Math.floor(Math.random() * (3 - 1)) + 1

        while (this.oponentOne.health > 0 || this.oponentTwo.health > 0) {
            const oponentOneAttack = this.oponentOne.theAttack
            const oponentTwoAttack = this.oponentTwo.theAttack

            if (num === 1) {


                history.push({num_attack: numberAttack, name: this.oponentOne.name, attack: oponentOneAttack, rest_health: this.oponentTwo.health, adversaire: this.oponentTwo.name })
                this.oponentTwo.health = this.oponentTwo.health - oponentOneAttack;
                if (this.oponentTwo.health <= 0) {
                    winner.push({winner: this.oponentOne.name})
                    break;
                }
                history.push({num_attack: numberAttack, name: this.oponentTwo.name, attack: oponentTwoAttack, rest_health: this.oponentTwo.health, adversaire: this.oponentOne.name })
                this.oponentTwo.health = this.oponentTwo.health - oponentOneAttack;
                if (this.oponentOne.health <= 0) {
                    winner.push({winner: this.oponentTwo.name})
                    break
                }
                numberAttack++
            } else if (num === 2) {

                history.push({num_attack: numberAttack, name: this.oponentTwo.name, attack: oponentTwoAttack, rest_health: this.oponentTwo.health, adversaire: this.oponentOne.name })
                this.oponentOne.health = this.oponentOne.health - oponentTwoAttack;
                if (this.oponentOne.health <= 0) {
                    winner.push({winner: this.oponentOne.name})
                    break
                }

                history.push({num_attack: numberAttack, name: this.oponentOne.name, attack: oponentOneAttack, rest_health: this.oponentOne.health, adversaire: this.oponentTwo.name })
                this.oponentTwo.health = this.oponentTwo.health - oponentOneAttack;
                if (this.oponentTwo.health <= 0) {
                    winner.push({winner: this.oponentTwo.name})
                    break;
                }
                numberAttack++
            }

        }

        return [history, winner];
    }
}





