import {Enemy} from '../Enemy';

export class Golem extends Enemy {
    constructor(name: string) {
        super(name, 100, 10, 1, 1);
        this.name = name;
        this.theAttack = this.attack()
    }

}
