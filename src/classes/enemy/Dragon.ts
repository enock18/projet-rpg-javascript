import {Enemy} from '../Enemy';

export class Dragon extends Enemy {
    protected flying: boolean;

    constructor(name: string) {
        super(name, 100, 10, 1, 1);
        this.name = name;
        this.theAttack = this.fly(Boolean(Math.round(Math.random()))) !== false ? this.attackFromSky() : this.attack()
    }

    public fly(value: boolean): boolean {
        this.flying = value;
        return this.flying
    }

    public attackFromSky(): number {
        const powerUp = ((this.hitStrength * this.lvl) * 10) / 100
        this.theAttack = (this.hitStrength * this.lvl) + powerUp
        return (this.hitStrength * this.lvl) + powerUp
    }
}

