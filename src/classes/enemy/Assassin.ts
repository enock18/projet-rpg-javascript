import {Enemy} from '../Enemy';

export class Assassin extends Enemy {

    constructor(name: string) {
        super(name, 100, 10, 1, 1);
        this.name = name;
        this.theAttack = this.attack()
    }

    public attack(): number {
        const assassinAttack = ((this.hitStrength * this.lvl) * 10) / 100;
        this.theAttack = assassinAttack + (this.hitStrength * this.lvl);
        return assassinAttack + (this.hitStrength * this.lvl);
    }
}
