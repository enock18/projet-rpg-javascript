import {Enemy} from '../Enemy';

export class Berserker extends Enemy {
    constructor(name: string) {
        super(name, 100, 10, 1, 54);
        this.name = name;
        this.theAttack = this.attack()
    }
}
