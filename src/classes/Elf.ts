import { Hero } from './Hero';

export class Elf extends Hero {

    constructor(name: string) {
        super(name, 100, 10, 1, 1);
        this.name = name;
        this.theAttack = this.attack()
    }

}