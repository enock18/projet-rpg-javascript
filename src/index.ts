import {Assassin} from './classes/enemy/Assassin';
import {Berserker} from './classes/enemy/Berserker';
import {Dragon} from "./classes/enemy/Dragon";
import { Battle } from './classes/Battle';
import {Griffin} from "./classes/enemy/Griffin";
import {Golem} from "./classes/enemy/Golem";
import {Werewolf} from "./classes/enemy/Werewolf";
import {Human} from "./classes/Human";
import {Elf} from "./classes/Elf";
import {Dwarf} from "./classes/Dwarf";



const startFight:any = () => {
const op1Name = (document.getElementById("op1") as HTMLInputElement).value
const op2Name = (document.getElementById("op2") as HTMLInputElement).value
const opOneRace = (document.getElementById("Hero") as HTMLInputElement).value
const opTwoRace = (document.getElementById("Enemy") as HTMLInputElement).value


const dataOp1 = document.getElementById("data-op1")
dataOp1.innerHTML = ""
const dataOp2 = document.getElementById("data-op2")
dataOp2.innerHTML = ""
const action = document.getElementById("action")
action.innerHTML = `<caption>${opOneRace.toUpperCase()} vs ${opTwoRace.toUpperCase()}</caption>
<tr>
    <th>Tour</th>
    <th>Nom perso</th>
    <th>Attaque</th>
    <th>Conséquences</th>
</tr>`
const winner = document.getElementById("winner")



switch (opOneRace){
    case "dragon": var opOne = new Dragon(op1Name);break;
    case "golem" : var opOne = new Golem(op1Name);break;
    case "griffin" : var opOne = new Griffin(op1Name);break;
    case "berserker" : var opOne = new Berserker(op1Name);break;
    case "werewolf" : var opOne = new Werewolf(op1Name);break;
    case "assassin" : var opOne = new Assassin(op1Name);break;
    case "human" : var opOne = new Human(op1Name);break;
    case "dwarf" : var opOne = new Dwarf(op1Name);break;
    case "elf" : var opOne = new Elf(op1Name);break;
}

switch (opTwoRace) {
    case "dragon": var opTwo = new Dragon(op2Name);break;
    case "golem" : var opTwo = new Golem(op2Name);break;
    case "griffin" : var opTwo = new Griffin(op2Name);break;
    case "berserker" : var opTwo = new Berserker(op2Name);break;
    case "werewolf" : var opTwo = new Werewolf(op2Name);break;
    case "assassin" : var opTwo = new Assassin(op2Name);break
}

dataOp1.innerHTML +=`<th>${opOne.getName()}</th>
<th>${opOneRace}</th>
<th>${opOne.getHealth()}</th>
<th>${opOne.getLevel()}</th>
<th>${opOne.getXp()}</th>`

dataOp2.innerHTML +=`<th>${opTwo.getName()}</th>
<th>${opTwoRace}</th>
<th>${opTwo.getHealth()}</th>
<th>${opTwo.getLevel()}</th>
<th>${opTwo.getXp()}</th>`

    const battle = new Battle(opOne, opTwo)
    battle.fightStart()[0].map( e => {
        action.innerHTML += `<tr>
         <td>${e.num_attack}</td>
        <td>${e.name}</td>
        <td>${e.name.toUpperCase()} attaque ${e.adversaire.toUpperCase()} avec une force de ${e.attack}</td>
        <td>${e.adversaire.toUpperCase()} recoit ${e.attack} de degat, sa vie passe a ${e.rest_health}</td>
        </tr>`
    })
    battle.fightStart()[1].map( e => {
        winner.innerHTML += `<tr>
        <td>${e.winner.toUpperCase()}</td>
      </tr>`
    })


    console.log(battle.fightStart())
}

document.getElementById("start").addEventListener("click", startFight);


